// npm install  ts-node typescript @types/node
// npm install express @types/express
// npm install ts-node-dev
// npm install express-session @types/express-session
// npm install socket.io @types/socket.io

// @ts-nocheck
import express from 'express';
import {Request,Response} from 'express';
import expressSession from 'express-session';
import http from 'http';
import cors from "cors"
import {Server as SocketIO} from 'socket.io';

const app = express();
app.use(cors())

app.use(express.urlencoded({extended:true}));
app.use(express.json());

const server = new http.Server(app);
const io = new SocketIO(server,{
    cors: {
      origin: '*',
      methods: ['GET', 'POST']
    }
});

const sessionMiddleware = expressSession({
    secret: 'This is the secret yeah',
    resave:true,
    saveUninitialized:true,
    cookie:{secure:false}
});

app.use(sessionMiddleware);

let numInc = 0;
let onlinePplCountArr = [];

io.use((socket,next)=>{
    let req = socket.request as express.Request
    let res = req.res as express.Response
    sessionMiddleware(req, res, next as express.NextFunction)
});

io.on('connection', async function (socket) {

    const req = socket.request as express.Request;

    if(!req.session['user']){
        req.session['user'] = "user" + ++numInc ;
    }

    onlinePplCountArr.push(req.session['user']);
    console.log(req.session['user'], "enter the server.");

    socket.request.session.save();

    if(socket.request.session['user']){
        socket.join(`user-${socket.request.session['user']}`);
    }

    socket.on('ioSendMsg' , (data) => { ioSendMsg(data, socket) } );

    // For specific connected user (socket.xxxxx)
    socket.emit('getUserId',req.session['user']);

    // For all users connected (io.xxxxx)  
    io.emit('onlineppl', onlinePplCountArr);

    //let arr = await allCurrentUserList(req);
    
    socket.on("disconnect",()=>{
        console.log(req.session['user'], "leave the server.");
        onlinePplCountArr = onlinePplCountArr.filter( v => v !== req.session['user']);
        io.emit('onlineppl', onlinePplCountArr);
    })

    //console.log(socket);
});

async function allCurrentUserList(req){
    let arr = [];
    req.sessionStore.all((err, sessions)=>{

        for (const [key, value] of Object.entries(sessions)) {
            arr.push(value.user);
        }

        console.log(arr)

        return arr;
        
    });

}

//socket.request.session['user']
async function ioSendMsg(data, socket){

    try{

        // current user no session
        if(!socket.request.session['user']){
            throw new Error("no current user");
        }

        let target = data.user;
        let message = data.message;
  
        console.log(socket.request.session['user'], "send to", target);
     
        let name = "user-" + target;

        if( !io.sockets.adapter.rooms.has(name) ){
            throw new Error("no target user room");
        }
            
        io.to(`user-${target}`).emit("reciveData", {"from" :socket.request.session['user'], "msg":message});

        socket.emit('sendSuccess',{ "status" : "ok" });
        
        return;
    }
    catch(e){
        socket.emit('sendSuccess',{ "status" : "bad" });
        return;
    }

}


app.get("/testApple", (req,res) =>{

    try{

        if(req.session){
            io.to(`user-${req.session['user']}`).emit("new-apple","Congratulations! New Apple Created!");
        }
    
        res.json({updated:1});

    }
    catch{

        res.json({updated:0});

    }

    

})

// express version for using io.emit
app.get("/sendMsgToUser", (req,res) =>{
    try{

        if(!req.session['user']){
            throw new Error("no current user");
        }

        let target = req.query.user;
        let message = req.query.message;
        let from = req.query.from;
  
        console.log(req.session['user'], "send to", target);
    
        if(req.session){
            let name = "user-" + target;

            if( !io.sockets.adapter.rooms.has(name) ){
                throw new Error("no target user");
            }
            
            io.to(`user-${target}`).emit("reciveData", {"from" :from, "msg":message});
        }
    
        res.json({updated:"ok"});
    }
    catch(e){
        res.json({updated:"bad"});
    }

})



app.use(express.static('public'))


const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});