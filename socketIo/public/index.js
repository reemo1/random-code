const socket = io.connect();

const userName = document.getElementById("userName");
const messageBox = document.getElementById("messageBox");
const onlinePplDisplay = document.getElementById("onlinePplDisplay");

const sendContent = document.getElementById("sendContent");
const targetUser = document.getElementById("targetUser");
const sendBtn = document.getElementById("sendBtn");

let myId = null;

// send msg
socket.emit("get-students","Hello world");

// Recive id
socket.on('getUserId',(data)=>{
    console.log(data);
    myId = data;
    userName.innerHTML = data;
})

// Recive user message
socket.on('reciveData',(data)=>{
    messageBox.innerHTML += `<h4>From ${data.from}: ${data.msg}</h4>`
});

// Recive send success status
socket.on('sendSuccess',(data)=>{

    messageBox.innerHTML += data.status === "ok" ?
    `<h4 style="text-align: right;">To ${targetUser.value}: ${sendContent.value}</h4>` :
    `<h4 style="text-align: right;">Send Falied.</h4>`;

    sendContent.value = "";
});

// Recive online users list
socket.on('onlineppl',(data)=>{
    onlinePplDisplay.innerHTML = `<h1>Online users:(${data.length}) ${data}</h1>`
});


// Not a good approach to use fetch get method, but for demo uses
//let res = await fetch(`http://localhost:8080/sendMsgToUser?user=${user}&message=${message}&from=${from}`);
//let result = await res.json();

sendBtn.addEventListener("click", async (e) =>{

    if(!sendContent.value || !targetUser.value){
        return;
    }

    socket.emit("ioSendMsg" ,{ "message": sendContent.value, "user" : targetUser.value }); 

})

// async function getApp(){
//     let res = await fetch("http://localhost:8080/testApple?livingRoom=true&price=<3000");
//     let result = await res.json();
//     console.log(result);
// }

let ser = "SELECT * FROM table WHERE"
//bedroom = true, price < 5000, livingroom = true
ser += "bedroom = true"
ser += "AND price < 5000"
ser += "AND livingroom = true"