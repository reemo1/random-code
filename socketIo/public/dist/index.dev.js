"use strict";

var socket = io.connect();
var userName = document.getElementById("userName");
var messageBox = document.getElementById("messageBox");
var onlinePplDisplay = document.getElementById("onlinePplDisplay");
var sendContent = document.getElementById("sendContent");
var targetUser = document.getElementById("targetUser");
var sendBtn = document.getElementById("sendBtn");
var myId = null; // send msg

socket.emit("get-students", "Hello world"); // Recive id

socket.on('getUserId', function (data) {
  console.log(data);
  myId = data;
  userName.innerHTML = data;
}); // Recive user message

socket.on('reciveData', function (data) {
  messageBox.innerHTML += "<h4>From ".concat(data.from, ": ").concat(data.msg, "</h4>");
}); // Recive send success status

socket.on('sendSuccess', function (data) {
  messageBox.innerHTML += data.status === "ok" ? "<h4 style=\"text-align: right;\">To ".concat(targetUser.value, ": ").concat(sendContent.value, "</h4>") : "<h4 style=\"text-align: right;\">Send Falied.</h4>";
  sendContent.value = "";
}); // Recive online users list

socket.on('onlineppl', function (data) {
  onlinePplDisplay.innerHTML = "<h1>Online users:(".concat(data.length, ") ").concat(data, "</h1>");
}); // Not a good approach to use fetch get method, but for demo uses
//let res = await fetch(`http://localhost:8080/sendMsgToUser?user=${user}&message=${message}&from=${from}`);
//let result = await res.json();

sendBtn.addEventListener("click", function _callee(e) {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (!(!sendContent.value || !targetUser.value)) {
            _context.next = 2;
            break;
          }

          return _context.abrupt("return");

        case 2:
          socket.emit("ioSendMsg", {
            "message": sendContent.value,
            "user": targetUser.value
          });

        case 3:
        case "end":
          return _context.stop();
      }
    }
  });
}); // async function getApp(){
//     let res = await fetch("http://localhost:8080/testApple?livingRoom=true&price=<3000");
//     let result = await res.json();
//     console.log(result);
// }

var ser = "SELECT * FROM table WHERE"; //bedroom = true, price < 5000, livingroom = true

ser += "bedroom = true";
ser += "AND price < 5000";
ser += "AND livingroom = true";