import { IonButton, IonAlert } from '@ionic/react';
import { useEffect, useState } from "react"

import { Map, Marker, ZoomControl, Draggable } from "pigeon-maps" //https://pigeon-maps.js.org/
import { Geolocation } from '@capacitor/geolocation'; //https://capacitorjs.com/docs/apis/geolocation

import ppl from '../imgs/ppl.svg'

//Thanks https://stackoverflow.com/questions/36862334/get-viewport-window-height-in-reactjs/36862446#36862446
function getWindowDimensions() {
    const { innerWidth: width, innerHeight: height } = window;
    return {
      width,
      height
    };
}

const printCurrentPosition = async () => {
    const coordinates = await Geolocation.getCurrentPosition();
    console.log('Current position:', coordinates);
    return coordinates
};

function LeafMap(){

    const [ windowDimensions, setWindowDimensions ] = useState(getWindowDimensions());

    const [ currLocation , setCurrLocation ] = useState<[number, number]>([0,0]); // your gps location
    const [ center, setCenter ] = useState<[number, number]>([22.302711, 114.177216]); // general center location, where [22.302711,114.177216] in HK location

    const [ zoom, setZoom ] = useState<number>(11);
    const [ showAlert, setShowAlert ] = useState<boolean>(false);

    const [ dragObjectAnchor, setDragObjectAnchor ] = useState<[number, number]>([22.302711, 114.177216]);

    useEffect(() => { // resize handler
        const handleResize = () => setWindowDimensions(getWindowDimensions());
        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);


    useEffect( () => { // get current location ONE time
        ( async () => {
            let result = await printCurrentPosition() as any;
            setCurrLocation([result.coords.latitude, result.coords.longitude])
        })()
    },[])

    //goto current location
    function gotoCurrentLocation(){
        setZoom(16);
        setCenter(currLocation);
    }

    return(
        <>
        <IonAlert
          isOpen={showAlert}
          onDidDismiss={() => setShowAlert(false)}
          cssClass='my-custom-class'
          header={'Hello youtube'}
          subHeader={'burrrrrrrrrrr'}
          message={'This is one midtown.'}
          buttons={['OK']}
        />

        <div style={{ textAlign: "right"}}>
            <IonButton color="dark" onClick={ () => gotoCurrentLocation()}>Go Current location</IonButton>
        </div>

        <Map height={windowDimensions.height * 0.90} 
            defaultCenter={[22.302711,114.177216]} 
            defaultZoom={zoom}
            zoom={zoom}
            center={center} 
            onBoundsChanged={({ center, zoom }) => { 
                setCenter(center) 
                setZoom(zoom) 
            }} 
        >
            <ZoomControl />

            <Draggable offset={[60, 87]} anchor={dragObjectAnchor} onDragEnd={setDragObjectAnchor}>
                <img src={ppl} width={150} height={150} alt="ppl..." />
            </Draggable>

            <Marker color={"red"} width={150} anchor={[22.372622681902595, 114.10824351800245]} onClick={ () => setShowAlert(true) }/>
            <Marker width={50} anchor={currLocation} />
        </Map>
        </>
    )
}

export default LeafMap