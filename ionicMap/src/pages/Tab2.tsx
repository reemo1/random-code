import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';

import LeafMap from '../components/LeafMap';

const Tab2: React.FC = () => {
  return (
    <IonPage>
      
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 2</IonTitle>
        </IonToolbar>
      </IonHeader>

      <LeafMap />

      
    </IonPage>
  );
};

export default Tab2;
